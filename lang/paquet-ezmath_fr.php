<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezmashup.git
return [
	// E
	'ezmashup_description' => '',
	'ezmashup_nom' => 'Librairie Mathématique & Statistique',
	'ezmashup_slogan' => 'Faciliter la mise en œuvre de fonctions mathématiques et statistiques',
];
