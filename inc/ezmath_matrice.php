<?php
/**
 * Ce fichier contient l'API de manipulation de matrices
 *
 * @package SPIP\EZMATH\MATRICE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Effectue de produit de deux matrices compatibles.
 * Les matrices sont réputées être dans l'ordre "column-major" à savoir la première matrice est d'ordre m * n
 * et la seconde d'ordre n * p.
 *
 * @param array $matrice1
 * @param array $matrice2
 *
 * @return array La matrice produit ou tableau vide si les matrices ne sont pas compatibles
 */
function matrice_multiplier(array $matrice1, array $matrice2) : array {
	// Initialiser les dimensions des deux matrices
	$dimensions1 = matrice_dimensionner($matrice1);
	$dimensions2 = matrice_dimensionner($matrice2);

	// On vérifie que les matrices sont correctement formées et qu'elles sont bien dans l'ordre column-major
	$produit = [];
	if (
		$dimensions1['cols']
		and $dimensions2['cols']
		and ($dimensions1['cols'] === $dimensions2['rows'])
	) {
		// Calcul du produit des matrices
		for ($i = 0; $i < $dimensions1['rows']; $i++) {
			for ($j = 0; $j < $dimensions2['cols']; $j++) {
				for ($k = 0; $k < $dimensions1['cols']; $k++) {
					if (!isset($produit[$i][$j])) {
						$produit[$i][$j] = 0;
					}
					$produit[$i][$j] += $matrice1[$i][$k] * $matrice2[$k][$j];
				}
			}
		}
	}

	return $produit;
}

/**
 * Transpose une matrice.
 *
 * @param array $matrice
 *
 * @return array La matrice transposée
 */
function matrice_transposer(array $matrice) : array {
	// Initialiser les dimensions des deux matrices
	$dimensions = matrice_dimensionner($matrice);

	// On vérifie que les matrices sont correctement formées et qu'elles sont bien dans l'ordre column-major
	$transposee = [];
	if ($dimensions['cols']) {
		// Calcul de la transposition
		for ($i = 0; $i < $dimensions['cols']; $i++) {
			for ($j = 0; $j < $dimensions['rows']; $j++) {
				$transposee[$i][$j] = $matrice[$j][$i];
			}
		}
	}

	return $transposee;
}

/**
 * @param array $matrice
 *
 * @return array
 */
function matrice_dimensionner(array $matrice) : array {
	// La matrice est à minima un vecteur sinon une erreur fatale sera levée par PHP : on calcule le nombre de lignes
	$nb_lignes = count($matrice);

	// On calcule le nombre de colonnes
	$nb_colonnes = 0;
	if (isset($matrice[0])) {
		$nb_colonnes = count($matrice[0]);
		// On verifie que toutes les lignes ont la même taille
		for ($i = 1; $i < $nb_colonnes; $i++) {
			if (count($matrice[$i]) !== $nb_colonnes) {
				// On remet à zéro le nombre colonnes pour lever une erreur
				$nb_colonnes = 0;
				break;
			}
		}
	}

	return [
		'rows' => $nb_lignes,
		'cols' => $nb_colonnes
	];
}
