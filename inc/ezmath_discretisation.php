<?php
/**
 * Ce fichier contient les fonctions d'API permettant la discrétisation d'une série de données quantitatives.
 *
 * @package SPIP\EZMATH\DISCRETISATION\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EZMATH_TAI_METHODE')) {
	/**
	 * Indique la méthode de calcul du TAI avec la moyenne ou la valeur centrale (défaut `moyenne`).
	 * Correspond au choix de la variable μₖ dans la formule.
	 */
	define('_EZMATH_TAI_METHODE', 'moyenne');
}

// ------------------------------------------------------------------
// ---------------------- INDICES DE CLASSE -------------------------
// ------------------------------------------------------------------

/**
 * Calcule l'indice de Hunstberger qui permet de définir le nombre de classes optimale pour la discrétisation
 * d'une série de valeurs en utilisant une formule basée uniquement sur l'effectif.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function serie_indice_huntsberger(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		$indice = round(1 + 3.3 * log10(count($serie)), 2);
	}

	return $indice;
}

/**
 * Calcule l'indice de Brooks qui permet de définir le nombre de classes optimale pour la discrétisation
 * d'une série de valeurs en utilisant une formule basée uniquement sur l'effectif.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function serie_indice_brooks(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		$indice = round(5 * log10(count($serie)), 2);
	}

	return $indice;
}

/**
 * Calcule l'indice de Yule qui permet de définir le nombre de classes optimale pour la discrétisation
 * d'une série de valeurs en utilisant une formule basée uniquement sur l'effectif.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function serie_indice_yule(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		$indice = round(2.5 * pow(count($serie), 1 / 4), 2);
	}

	return $indice;
}

/**
 * Calcule l'indice de Brooks qui permet de définir le nombre de classes optimale pour la discrétisation
 * d'une série de valeurs en utilisant une formule basée sur l'effectif, l'étendue et l'écart type.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function serie_indice_scott(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		include_spip('inc/ezmath_statistique');
		$indice = round(serie_etendue($serie) / (3.5 * serie_ecart_type($serie) * pow(count($serie), -1 / 3)), 2);
	}

	return $indice;
}

/**
 * Calcule l'indice de Brooks qui permet de définir le nombre de classes optimale pour la discrétisation
 * d'une série de valeurs en utilisant une formule basée sur l'effectif, l'étendue et la distance interquartile.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function serie_indice_diaconis(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		include_spip('inc/ezmath_statistique');
		$indice = round(serie_etendue($serie) / (2 * serie_interquartile($serie) * pow(count($serie), -1 / 3)), 2);
	}

	return $indice;
}

// ---------------------------------------------------------------
// ---------------------- DISCRETISATION -------------------------
// ---------------------------------------------------------------

/**
 * Discrétise une série de valeurs en un nombre de classes donné en appliquant une méthode de discrétisation
 * choisie parmi les méthodes mathématiques ou statistiques classiques et, eventuellement, en transformant
 * mathématiquement la série pour la normaliser.
 *
 * Les méthodes mathématiques disponibles sont :
 * - discrétisation en classes d'égales étendues (`equivalence`)
 * - discrétisation selon une progression arithmétique (`progression_arithmetique`)
 * - discrétisation selon une progression géométrique (`progression_geometrique`)
 *
 * Les méthodes statistiques disponibles sont :
 * - discrétisation selon les quantiles (`quantile`)
 * - discrétisation selon la moyenne et l'écart-type (`standard`)
 * - discrétisation selon les moyennes emboîtées (`moyenne_emboitee`)
 * - discrétisation de Jenks basée sur la notion de variance (`jenks`)
 *
 * Pour toutes les méthodes sauf celle de Jenks, les intervalles de classes sont fermés à gauche et ouverts à droite.
 * Dans le cas de la méthode de Jenks, c'est le contraire.
 *
 * Les transformations mathématiques autorisées sont : puissance, racine, logarithme, box-cox, 1/x et les fonctions
 * inverses.
 *
 * @param array       $serie          Tableau des valeurs de la série.
 * @param null|string $methode        Identifiant de la méthode de discrétisation (par défaut, égales étendues)
 * @param null|int    $nb_classes     Nombre de classes de la discrétisation (défaut 5 sauf pour les moyennes emboitées)
 * @param null|array  $transformation Opération mathématique à appliquer à la valeur décrite sous forme de tableau :
 *                                    - string `fonction`  : fonction mathématique PHP à appliquer
 *                                    - float  `parametre` : le parametre éventuel de la fonction PHP autre que la valeur (null sinon)
 *                                    - bool   `inverser`  : indique si on doit inverser la valeur transformée
 *
 * @return array Tableau des classes et de la série discrétisée, à savoir :
 *               - index `serie_classe` : la série avec les index conservé et les valeurs remplacées par l'index de la classe d'appartenance
 *               - index `classes` : La liste des classes avec pour chaque classe,
 *               - `binf` et `bsup`, les bornes de l'intervalle
 *               - `effectif` et `frequence`, le nombre d'éléments et la fréquence
 *               - `centre`, la valeur centrale de la classe
 *               - `etendue` et `etendue_ponderee`, l'étendue et l'étendue pondérée par la moyenne de la classe
 *               - `serie`, le tableau des éléménts de la série inclus dans la classe
 *               - index `qualite` : les indicateurs de la qualité de la discrétisation (TAI, I, et R)
 */
function serie_discretisation(array $serie, ?string $methode = 'equivalence', ?int $nb_classes = 5, ?array $transformation = []) : array {
	// Initialisation de la sortie avec un liste de classes à vide, ce qui permet d'identifier une erreur
	$discretisation = [
		'serie_classe' => [],
		'classes'      => [],
		'classe_vide'  => false,
	];

	if (!empty($serie)) {
		// Transformation de la série si demandé
		$serie_transformee = $transformation ? serie_transformer($serie, $transformation) : $serie;

		// Construction des classes
		// -- identification de la fonction de construction des classes par la méthode choisie et vérification de son existence
		$classer = "classe_{$methode}";
		$classes = function_exists($classer) ? $classer($serie_transformee, $nb_classes) : [];

		// Discrétisation de la série et finalisation des attributs des classes
		if (!empty($classes)) {
			//Si la série a été transformée, on rétablit les bornes de classe dans le référentiel initial de la série
			$bornes_inversees = false;
			if ($transformation) {
				for ($i = 0; $i < $nb_classes; $i++) {
					// Traitement de l'effectif et de la série qui peuvent être vides
					// -- suivant la transformation, il est possible que les bornes s'inversent : il faut donc le gérer
					$binf_transformee = valeur_transformer($classes[$i]['binf'], $transformation, true);
					$bsup_transformee = valeur_transformer($classes[$i]['bsup'], $transformation, true);
					$classes[$i]['binf'] = min($binf_transformee, $bsup_transformee);
					$classes[$i]['bsup'] = max($binf_transformee, $bsup_transformee);
					// -- si inversion on le consigne pour inverser le tableau in fine
					if ($i === 0) {
						$bornes_inversees = $binf_transformee > $bsup_transformee;
					}
				}

				// Si besoin on remet la liste des classes dans l'ordre croissant
				if ($bornes_inversees) {
					$classes = array_reverse($classes);
				}

				// S'assurer que les bornes min et max soient bien celles de la série pour éviter une comparaison érronée
				// due au fait que les bornes transformées peuvent être différentes à la décimale près
				$classes[array_key_first($classes)]['binf'] = min($serie);
				$classes[array_key_last($classes)]['bsup'] = max($serie);
			}

			// Définir le mode d'inclusion dans les classes :
			// -- pour la méthode de Jenks : on inclut la borne supérieure
			// -- pour toutes les autres méthodes : on inclut la borne inférieure
			$inclusion = strpos($methode, 'jenks') !== false ? 'bsup' : 'binf';

			// Discrétiser la série en fonction des bornes des classes et compter les éléments dans chaque classe
			// -- cette boucle est plus performante que si on l'intégrait dans la boucle précédente qui est forcément en np.
			$classe_vide_existe = false;
			$serie_discretisee = [];
			foreach ($serie as $_id => $_valeur) {
				$i = classe_trouver($_valeur, $classes, $inclusion);
				if (null !== $i) {
					// Pour la série :
					// -- établir la discrétisation de la série en indiquant l'index de classe correspondant à l'index de la série
					$serie_discretisee[$_id] = $i;

					// Pour la classe identifiée :
					// -- incrémenter l'effectif
					$classes[$i]['effectif'] = ($classes[$i]['effectif'] ?? 0) + 1;
					// -- Attacher l'élement de la série à la classe
					if (!isset($classes[$i]['serie'])) {
						$classes[$i]['serie'] = [];
					}
					$classes[$i]['serie'][$_id] = $_valeur;
				}
			}
			$discretisation['serie_classe'] = $serie_discretisee;

			// Finalisation des attributs des classes
			include_spip('inc/ezmath_statistique');
			for ($i = 0; $i < $nb_classes; $i++) {
				// Traitement de l'effectif et de la série qui peuvent être vides
				if (!isset($classes[$i]['effectif'])) {
					$classes[$i]['effectif'] = 0;
					$classe_vide_existe = true;
				}
				if (!isset($classes[$i]['serie'])) {
					$classes[$i]['serie'] = [];
				}

				$classes[$i]['centre'] = ($classes[$i]['bsup'] + $classes[$i]['binf']) / 2;
				$classes[$i]['etendue'] = ($classes[$i]['bsup'] - $classes[$i]['binf']);
				$classes[$i]['frequence'] = $classes[$i]['effectif'] / count($serie);
				$classes[$i]['moyenne'] = serie_moyenne($classes[$i]['serie']);
				$classes[$i]['serie_min'] = $classes[$i]['serie'] ? min($classes[$i]['serie']) : null;
				$classes[$i]['serie_max'] = $classes[$i]['serie'] ? max($classes[$i]['serie']) : null;
				// Pour autoriser le calcul de l'indice D de Jenks :
				// -- on met zéro à l'étendue pondérée si l'effectif est nul mais cela minimise l'indice
				$classes[$i]['etendue_ponderee'] = $classes[$i]['moyenne']
					? $classes[$i]['etendue'] / $classes[$i]['moyenne']
					: 0;
				$classes[$i]['etendue_ponderee_theorique'] = $classes[$i]['etendue'] / $classes[$i]['centre'];
			}
			$discretisation['classes'] = $classes;

			// Mise à jour de l'indicateur d'existence de classe vide
			if ($classe_vide_existe) {
				$discretisation['classe_vide'] = true;
			}
		}
	}

	return $discretisation;
}

/**
 * Construit, pour une série de valeur, la liste des classes d'égales étendues.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_equivalence(array $serie, ?int $nb_classes = 5) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la plage des valeurs
		$min = min($serie);
		$max = max($serie);

		// Calcul de la largeur de chaque classe
		$taille_classe = ($max - $min) / $nb_classes;

		// Créer les limites des classes, l'effectif sera complété lors de la discrétisation
		for ($i = 0; $i < $nb_classes; $i++) {
			// Créer les bornes des classes, l'effectif sera complété lors de la discrétisation
			$classes[] = [
				'binf' => $min + $i * $taille_classe,
				'bsup' => $min + ($i + 1) * $taille_classe
			];
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes selon une progression arithmétique.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_progression_arithmetique(array $serie, ?int $nb_classes = 5) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la plage des valeurs
		$min = min($serie);
		$max = max($serie);

		// Calcul de la largeur de chaque classe : (max - min) / Somme(1 à nb_classes)
		// sachant que Somme(1 à nb_classes) = (nb_classes * (nb_classes + 1)) / 2
		$taille_classe = 2 * ($max - $min) / ($nb_classes * ($nb_classes + 1));

		// Créer les limites des classes, l'effectif sera complété lors de la discrétisation
		$borne_courante = $min;
		for ($i = 0; $i < $nb_classes; $i++) {
			// On initialise la classe en remplissant uniquement la borne inférieure
			$classe = [
				'binf' => $borne_courante
			];

			// On met à jour la borne courante qui va devenir la borne supérieure de la classe en cours de création
			$borne_courante += ($i + 1) * $taille_classe;
			$classe['bsup'] = $borne_courante;

			// On stocke la classe
			$classes[] = $classe;
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes selon une progression géométrique.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_progression_geometrique(array $serie, ?int $nb_classes = 5) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la plage des valeurs
		$min = min($serie);
		$max = max($serie);

		// Calcul de la largeur de chaque classe : log(10, X) = (log(10, $max) - log(10, $min)) / nb_classes
		// $taille_classe = pow($max / $min, 1 / $nb_classes);
		$taille_classe = pow(10, (log10($max) - log10($min)) / $nb_classes);

		// Créer les limites des classes, l'effectif sera complété lors de la discrétisation
		$borne_courante = $min;
		for ($i = 0; $i < $nb_classes; $i++) {
			// On initialise la classe en remplissant uniquement la borne inférieure
			$classe = [
				'binf' => $borne_courante
			];

			// On met à jour la borne courante qui va devenir la borne supérieure de la classe en cours de création
			$borne_courante *= $taille_classe;
			$classe['bsup'] = $borne_courante;

			// On stocke la classe
			$classes[] = $classe;
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes basée sur les quantiles (effectifs égaux).
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_quantile(array $serie, ?int $nb_classes = 5) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	if (!empty($serie)) {
		// Récupérer la liste des quantiles en fonction du nombre de classes
		include_spip('inc/ezmath_statistique');
		$bornes = serie_quantiles($serie, $nb_classes);

		// Construction des classes avec bornes inférieure et supérieure et effectif initialisé à 0
		// On rajoute le min et le max de la série comme borne
		$bornes[] = min($serie);
		$bornes[] = max($serie);
		// -- on trie les bornes
		sort($bornes);
		// -- on limite la première et la dernière classe au min et au max de la série
		$nb_bornes = count($bornes);
		for ($i = 0; $i < $nb_bornes - 1; $i++) {
			$classes[] = [
				'binf' => $bornes[$i],
				'bsup' => $bornes[$i + 1]
			];
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes standardisées (moyenne et écart-type).
 *
 * @param array      $serie      Tableau des valeurs de la série.
 * @param null|int   $nb_classes Nombre de classes de la discrétisation (défaut 5)
 * @param null|float $k          Coefficient appliqué à l'écart-type pour adapter les classes (défaut 1)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_standard(array $serie, ?int $nb_classes = 5, ?float $k = 1) {
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la moyenne, de l'écart type et de l'effectif de la série
		include_spip('inc/ezmath_statistique');
		$moyenne = serie_moyenne($serie);
		$ecart_type = serie_ecart_type($serie);

		// On limite les bornes au min et au max de la série
		$min = min($serie);
		$max = max($serie);

		// Définir le nombre de classses à gauche et à droite de la moyenne
		$nb_classes_moyenne = floor($nb_classes / 2);
		if ($nb_classes % 2 == 0) {
			--$nb_classes_moyenne;
		}

		// Si le nombre de classes est pair, la moyenne est une borne, sinon c'est le centre d'une classe
		$bornes = $nb_classes % 2 == 0 ? [$moyenne] : [];

		// Générer les bornes en-dessous et au-dessus de la moyenne
		// -- on contraint les bornes à rester dans l'intervalle (min, max) de la série
		for ($i = 0; $i <= $nb_classes_moyenne; $i++) {
			// -- détermination des bornes à gauche et à droite de la moyenne
			$eloignement = $nb_classes % 2 == 0 ? $i + 1 : $i + 0.5;

			// -- calcul des bornes gauche et droite
			$borne_gauche = $moyenne - $eloignement * $ecart_type * $k;
			$borne_droite = $moyenne + $eloignement * $ecart_type * $k;

			// -- validation des bornes : on ne prend aucune borne hors intervalle (min, max)
			$bornes[] = max($borne_gauche, $min);
			$bornes[] = min($borne_droite, $max);
		}

		// Trier les limites de classes en ordre croissant
		sort($bornes);

		// Vérifier les bornes extrêmes
		// -- la borne max
		$index = count($bornes) - 1;
		if ($bornes[$index] < $max) {
			$bornes[$index] = $max;
		}
		// -- la borne min
		if ($bornes[0] > $min) {
			$bornes[0] = $min;
		}

		// Construction des classes avec bornes inférieure et supérieure
		$nb_bornes = count($bornes);
		for ($i = 0; $i < $nb_bornes - 1; $i++) {
			$classes[] = [
				'binf' => $bornes[$i],
				'bsup' => $bornes[$i + 1]
			];
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes standardisées (moyenne et écart-type) en adaptant
 * l'étendue de chaque classe pour que celles-ci restent incluses dans le domaine de la série.
 * Pour cela, on calcule un coefficient d'adaptation de l'écart-type toujours compris entre 0 et 1.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_standard_k(array $serie, ?int $nb_classes = 5) : array {
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la moyenne, de l'écart type de la série
		include_spip('inc/ezmath_statistique');
		$moyenne = serie_moyenne($serie);
		$ecart_type = serie_ecart_type($serie);

		// On identifie les bornes au min et au max de la série
		$min = min($serie);
		$max = max($serie);

		// Définir le nombre de classses à gauche et à droite de la moyenne
		$nb_classes_moyenne = floor($nb_classes / 2);

		// On définit un coefficient d'adaptation de l'étendue de chaque classe pour éviter de déborder
		// en dehors du domaine de la série
		$ecart_moyenne_min = min($moyenne - $min, $max - $moyenne);
		$etendue_max = $nb_classes % 2 == 0
			? $ecart_moyenne_min / $nb_classes_moyenne
			: $ecart_moyenne_min / ($nb_classes_moyenne + 0.5);
		$k = min(round($etendue_max / $ecart_type, 2), 1);

		// Appel de la fonction standard avec le cofficient déterminé
		// TODO : pouvoir renvoyer la valeur du k
		$classes = classe_standard($serie, $nb_classes, $k);
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes basée la méthode des cotes Z.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_standard_cote_z(array $serie, ?int $nb_classes = 5) : array {
	$classes = [];

	if (!empty($serie)) {
		// Calcul de la moyenne et de l'écart type
		include_spip('inc/ezmath_statistique');
		$moyenne = serie_moyenne($serie);
		$ecart_type = serie_ecart_type($serie);

		// Calcul de la cote Z pour chaque valeur
		$z_cotes = [];
		foreach ($serie as $_id => $_valeur) {
			$z_cotes[$_id] = ($_valeur - $moyenne) / $ecart_type;
		}

		// Définir la taille des classes dans le domaine des cotes Z
		$min_z = min($z_cotes);
		$max_z = max($z_cotes);
		$taille_z = ($max_z - $min_z) / $nb_classes;

		// Créer les bornes dans le domaine de la variable initiale, donc en utilisant la transformation inverse
		// de la cote Z, soit x = moyenne + z*ecart_type
		$bornes = [];
		for ($i = 0; $i <= $nb_classes; $i++) {
			$bornes[] = $moyenne + ($min_z + $i * $taille_z) * $ecart_type;
		}

		// Construction des classes avec bornes inférieure et supérieure
		$nb_bornes = count($bornes);
		for ($i = 0; $i < $nb_bornes - 1; $i++) {
			$classes[] = [
				'binf' => $bornes[$i],
				'bsup' => $bornes[$i + 1]
			];
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes basée sur les moyennes emboîtées.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 4)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_moyenne_emboitee(array $serie, ?int $nb_classes = 4) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	// On vérifie que le nombre de classes est bien une puissance de 2
	if (
		!empty($serie)
		&& ($nb_classes > 0)
		&& (($nb_classes & ($nb_classes - 1)) === 0)
	) {
		// Extraire la puissance de 2 qui correspond au nombre d'itérations
		$nb_iterations = (int) log($nb_classes, 2);

		// On trie la série pour faciliter le découpage selon les moyennes
		sort($serie);

		// On boucle autant de fois que la puissance en divisant les intervalles autour de la moyenne
		include_spip('inc/ezmath_statistique');
		$bornes = [];
		$series[0] = [$serie];
		for ($i = 1; $i <= $nb_iterations; $i++) {
			// On calcule la moyenne de toutes les sous-series et on positionne ces moyennes comme des bornes
			foreach ($series[$i - 1] as $_serie) {
				// On calcule la moyenne de la série et on insère cette moyenne dans le tableau des bornes
				$moyenne = serie_moyenne($_serie);
				$bornes[] = $moyenne;
				// On coupe la série en deux pour la prochaine itération si nécessaire
				if ($i < $nb_iterations) {
					$series[$i] = array_merge($series[$i] ?? [], serie_couper($_serie, $moyenne));
				}
			}
		}

		// Construction des classes avec bornes inférieure et supérieure et effectif initialisé à 0
		// On rajoute le min et le max de la série comme borne
		$bornes[] = min($serie);
		$bornes[] = max($serie);
		// -- on trie les bornes
		sort($bornes);
		// -- on limite la première et la dernière classe au min et au max de la série
		$nb_bornes = count($bornes);
		for ($i = 0; $i < $nb_bornes - 1; $i++) {
			$classes[] = [
				'binf' => $bornes[$i],
				'bsup' => $bornes[$i + 1]
			];
		}
	}

	return $classes;
}

/**
 * Construit, pour une série de valeur, la liste des classes basée sur les ruptures naturelles de Jenks.
 *
 * @param array    $serie      Tableau des valeurs de la série.
 * @param null|int $nb_classes Nombre de classes de la discrétisation (défaut 5)
 *
 * @return array Tableau des classes, à savoir :
 *               - index `binf`     : borne inférieure de l'intervalle
 *               - index `bsup`     : borne supérieure de l'intervalle
 */
function classe_jenks(array $serie, ?int $nb_classes = 5) : array {
	// Initialisation des classes à vide, ce qui permet d'identifier une erreur
	$classes = [];

	// On vérifie que le nombre de classes est bien une puissance de 2
	if (!empty($serie)) {
		// On fait appel à la librairie de xxx
		include_spip('lib/jenks');
		$bornes = Jenks::getBreaks($serie, $nb_classes);

		// Construction des classes avec bornes inférieure et supérieure et effectif initialisé à 0
		$nb_bornes = count($bornes);
		for ($i = 0; $i < $nb_bornes - 1; $i++) {
			$classes[] = [
				'binf' => $bornes[$i],
				'bsup' => $bornes[$i + 1]
			];
		}
	}

	return $classes;
}

// ------------------------------------------------------------------
// ---------------- INDICES QUALITE DISCRETISATION ------------------
// ------------------------------------------------------------------

/**
 * Calcule l'indice TAI de Jenks (Tabular Accuracy Index) qui permet d'évaluer l'homogénéité d'une discrétisation.
 * il mesure l'écart entre les valeurs de la série et les valeurs correspondantes des classes. Il compare les écarts
 * (en valeur absolue) des valeurs à la moyenne de chaque classe aux écarts de ces mêmes valeurs à la moyenne générale
 * de la série (toujours en valeur absolue).
 *
 *             K   Nₖ
 *             ∑   ∑ |xᵢₖ - μₖ|
 *            k=1 i=1
 * TAI = 1 - -------------------- où K désigne le nombre de classes, N l'effectif de la série, Nₖ l'effectif de la classe k,
 *               N                   xᵢₖ les valeurs xᵢ de la classe k, xᵢ les valeurs de la série,
 *               ∑ |xᵢ - μ|          μₖ la moyenne de la classe k et μ la moyenne globale de la série
 *              i=1
 *
 * - TAI ∈ [0, 1]
 * - TAI proche de 1 : la somme des écarts intra-classes est plus faible que la somme des écarts à la moyenne générale.
 *                     la discrétisation crée des classes homogènes (satisfaisant à partir de 0,8, excellent à partir de 0,9
 *                     et médiocre en dessous de 0,7)
 * - TAI proche de 0 : la somme des écarts intra-classes est peu différente de la somme des écarts à la moyenne générale.
 *                     la discrétisation crée des classes peu homogènes
 *
 * @param array       $serie   Tableau des valeurs de la série.
 * @param array       $classes Les classes issues de la discrétisation
 * @param null|string $methode `moyenne` (défaut) si μₖ est la moyenne arithmétique, `centre` si μₖ est le centre de la classe
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function discretisation_indice_tai(array $serie, array $classes, ?string $methode = _EZMATH_TAI_METHODE) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		// On calcule la moyenne arithmétique générale de la série
		include_spip('inc/ezmath_statistique');
		$moyenne = serie_moyenne($serie);

		// Calcul de la somme des écarts aux moyennes de chaque classe
		$ecart_classe = 0;
		foreach ($classes as $_classe) {
			foreach ($_classe['serie'] as $_id => $_valeur) {
				$ecart = ($methode === 'moyenne' ? $_classe['moyenne'] : $_classe['centre']) - $serie[$_id];
				$ecart_classe += abs($ecart);
			}
		}
		// Calcul des écarts à la moyenne générale
		$ecart_serie = 0;
		foreach ($serie as $_valeur) {
			$ecart = $moyenne - $_valeur;
			$ecart_serie += abs($ecart);
		}

		// Calcul de l'indice
		$indice = 1 - ($ecart_classe / $ecart_serie);
	}

	return $indice;
}

/**
 * Calcule l'indice I donné par le rapport des variances (ou des sommes carrées) intra et inter classes
 * qui donne une information sur l'homogénéité des classes.
 *
 *     σ²(intra)     SSW
 * I = ---------  = ----- où SSW est la somme des carrés intra-classes et SSB la somme des carrés inter-classes
 *     σ²(inter)     SSB
 *
 * - I ∈ [0,+∞[
 * - Plus I est proche de zéro, plus les groupes sont homogènes : excellent en dessous de 0,1, acceptable entre 0,1 et
 *   0,3 et médiocre au-delà.
 *
 * @param array $serie   Tableau des valeurs de la série.
 * @param array $classes Les classes issues de la discrétisation
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function discretisation_indice_i(array $serie, array $classes) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		// Calcul des variances intra-classes et inter-classes
		$sommes = discretisation_ssw_ssb($serie, $classes);
		if ($sommes) {
			// Calcul de l'indice
			// -- inutile de calculer les variances finales en les divisant par l'effectif de la série car l'indice
			//    est le rapport des deux ce qui élimine l'effectif. On calcule donc les sommes carrées
			$indice = $sommes['ssw'] / $sommes['ssb'];
		}
	}

	return $indice;
}

/**
 * Calcule l'indice de redondance (dérivé de l'entropie de Shannon) qui mesure le niveau d'information apporté par
 * la discrétisation.
 *
 *                  K
 *                  ∑ pᵢLog₂(pᵢ)
 *          H      k=1
 * R = 1 - ---- = --------------- où K désigne le nombre de classes et pᵢ la probabilité d'apparition des éléments
 *          Hₘ       Log₂(K)         dans une classe
 *
 * La probabilité d'apparition pᵢ est, par défaut, calculée à partir des effectifs de classe. Néanmoins, il est possible
 * de calculer cette probabilité en utilisant d'autres concepts comme la surface couverte par le phénomène.
 *
 * - R ∈ [0, 1], plus R est proche de zéro, plus l'information est importante (l'entropie se rapproche du max pour le nombre de
 *   classes choisi).
 * - R < 0.2 est généralement considéré comme le signe d'une bonne discrétisation.
 * - 0.2 ≤ R < 0.5, une certaine perte d'information est inévitable, mais la discrétisation est encore acceptable.
 * - R ≥ 0.5, une grande partie de l'information est perdue, et la discrétisation est probablement sous-optimale
 *
 * @param array      $serie        Tableau des valeurs de la série.
 * @param array      $classes      Les classes issues de la discrétisation.
 * @param null|array $probabilites Si non vide, fournit la probabilité pᵢ à utiliser pour chaque classe.
 *                                 Si vide, la probabilité correspond à la fréquence de chaque classe
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function discretisation_indice_redondance(array $serie, array $classes, ?array $probabilites = []) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($serie)) {
		// Si il y a des classes à effectif nul, on arrête le calcul
		$effectifs = array_column($classes, 'effectif');
		if (!in_array(0, $effectifs, true)) {
			// Calcul de l'entropie de la discrétisation
			$effectif = count($serie);
			$entropie = 0;
			foreach ($classes as $_no_classe => $_classe) {
				// La probabilité dépend de la méthode choisie
				$probabilite = empty($probabilites)
					? $_classe['effectif'] / $effectif
					: $probabilites[$_no_classe];
				$entropie -= $probabilite * log($probabilite, 2);
			}

			// Calcul de l'indice à partir de l'entropie et de l'entropie maximale pour le nombre de classes
			$indice = 1 - ($entropie / log(count($classes), 2));
		}
	}

	return $indice;
}

/**
 * Calcule l'indice D de Jenks qui permet d'évaluer la qualité d'une discrétisation.
 * Il compare les moyennes des classes observées à des moyennes estimées, attendues, en fonction des limites définies.
 * Cet indice mesure l'écart à l'intérieur de chaque classe : plus la somme des écarts est faible, plus l'indice est faible.
 *
 *      1
 * D = --- ∑|Epᵢ - εpᵢ| où K désigne le nombre de classes, Epᵢ l'étendue pondérée de la classe i,
 *      K               et εpᵢ l'étendue théorique pondérée de la classe i
 *
 * - D proche de 0 : la somme des écarts intra-classes est peu différente de la somme des écarts à la moyenne générale.
 * - D inférieur à 0,1 : excellent
 * - D entre 0,1 et 0,25 : acceptable
 * - D supérieur à 0,25 : médiocre
 *
 * @param array $classes Les classes issues de la discrétisation
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function discretisation_indice_d(array $classes) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($classes)) {
		// Calcul de la somme des écarts des étendues pondérées
		$indice = 0;
		foreach ($classes as $_classe) {
			$indice += abs($_classe['etendue_ponderee'] - $_classe['etendue_ponderee_theorique']);
		}

		// Pondérer avec le nombre de classes
		$indice = $indice / count($classes);
	}

	return $indice;
}

/**
 * Calcule l'indice C de Geary qui permet d'évaluer la qualité d'une discrétisation.
 * C'est le même calcul qui est fait sur la série - fonction serie_autocorrelation_geary() - mais en ne l'appliquant
 * que sur les classes et non la série elle-même.
 * L'indice obtenu est à comparer à celui de la série initiale.
 *
 * Plus l'indice est proche de celui de la série, plus la discrétisation sera considérée proche de la répartition initiale.
 * On peut dire :
 * - inférieur à 10% d'écart est très bon
 * - jusqu'à 25% c'est acceptable
 * - au-delà c'est médiocre, donc à rejeter
 *
 * @param array $classes     Les classes issues de la discrétisation
 * @param array $ponderation Matrice de pondération spatiale
 *
 * @return null|float Indice ou null si erreur (série vide)
 */
function discretisation_indice_c(array $classes, array $ponderation) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$indice = null;

	if (!empty($classes)) {
		// On reconstitue la série en affectant à chaque index la valeur du centre de la classe à laquelle elle appartient.
		$serie = [];
		foreach ($classes as $_classe) {
			foreach ($_classe['serie'] as $_id => $_valeur) {
				$serie[$_id] = $_classe['centre'];
			}
		}

		// Calcul de l'autocorrélation spatiale sur cette nouvelle série
		include_spip('inc/ezmath_statistique');
		$indice = serie_autocorrelation_geary($serie, $ponderation);
	}

	return $indice;
}

/**
 * Calcule la sommes des carrés intra et inter classes qui donne une information sur l'homogénéité des classes.
 * La somme des carrés intra-classes (SSW) correspond à la dispersion totale des observations autour de leurs moyennes
 * de classe respectives.
 * La somme des carrés inter-classes mesure la dispersion des moyennes des classes par rapport à la moyenne globale,
 * pondérée par la taille des classes.
 *
 *       K   Nₖ
 * SSW = ∑   ∑ (xᵢₖ - μₖ)² où K désigne le nombre de classes, Nₖ l'effectif de la classe k,
 *      k=1 i=1           xᵢₖ les valeurs xᵢ de la classe k, μₖ la moyenne de la classe k
 *                        et μ la moyenne globale de la série
 *       K
 * SSB = ∑ Nₖ(μₖ - μ)²
 *      k=1
 *
 * @param array $serie   Tableau des valeurs de la série.
 * @param array $classes Les classes issues de la discrétisation
 *
 * @return array Les sommes SSW et SSB ou tableau vide sinon (série vide)
 */
function discretisation_ssw_ssb(array $serie, array $classes) : array {
	// Exclure la série vide en renvoyant une erreur
	$sommes = [];

	if (!empty($serie)) {
		// On calcule la moyenne arithmétique générale de la série
		include_spip('inc/ezmath_statistique');
		$moyenne = serie_moyenne($serie);

		// Calcul de la variance intra-classes : en fait on ne calcule que les écarts quadratiques sans les diviser
		// par l'effectif de la série.
		$sommes['ssw'] = 0;
		$sommes['ssb'] = 0;
		foreach ($classes as $_classe) {
			foreach ($_classe['serie'] as $_id => $_valeur) {
				$sommes['ssw'] += pow($serie[$_id] - $_classe['moyenne'], 2);
			}
			$sommes['ssb'] += $_classe['effectif'] * pow($_classe['moyenne'] - $moyenne, 2);
		}
	}

	return $sommes;
}

// ------------------------------------------------------------------
// ------------------------- UTILITAIRES ----------------------------
// ------------------------------------------------------------------

/**
 * Détermine l'index de la classe (intervalle) dans laquelle se trouve une valeur.
 *
 * @param float|int   $valeur    Valeur numérique à localiser
 * @param array       $classes   Tableau des classes. L'intervalle est défini par les index `binf` et `bsup`.
 * @param null|string $inclusion Borne incluse dans l'intervalle, l'autre étant excluse. Vaut `binf` (défaut) ou `bsup`.
 *
 * @return null|int Index de la classe ou `null` si non trouvée
 */
function classe_trouver(int|float $valeur, array $classes, ?string $inclusion = 'binf') : null|int {
	// Si on ne trouve pas de classe, on renvoie null
	$index = null;

	$nb_classes = count($classes);
	$inclusion_binf = ($inclusion === 'binf');
	for ($i = 0; $i < $nb_classes; $i++) {
		// Calculer la condition sur la borne inférieure
		$condition_binf =
			$valeur > $classes[$i]['binf']
			|| (
				$valeur == $classes[$i]['binf']
				&& ($inclusion_binf || $i === 0)
			);

		// Calculer la condition sur la borne supérieure
		$condition_bsup =
			$valeur < $classes[$i]['bsup']
			|| (
				$valeur == $classes[$i]['bsup']
				&& (!$inclusion_binf || $i === $nb_classes - 1)
			);

		/// Vérification de l'inclusion de la valeur entre les bornes de la classe courante
		if (
			$condition_binf
			&& $condition_bsup
		) {
			$index = $i;
			break;
		}
	}

	return $index;
}

/**
 * Scinde une série de valeurs en deux séries de part et d'autre d'une valeur donnée.
 *
 * @param array     $serie        Tableau des valeurs de la série.
 * @param float|int $valeur_coupe Valeur permettant de construire les deux séries de part et d'autre
 *
 * @return array Tableau des séries résultantes, l'index `0` pour la série à gauche de la valeur de coupe et
 *               l'index `1` pour la série à droite. Renvoie vide si erreur.
 */
function serie_couper(array $serie, int|float $valeur_coupe) : array {
	$series = [];

	// On passe toutes les valeurs de la série jusqu'à trouver l'index le plus proche de la valeur de coupe.
	// -- pour améliorer les performances, on constitue en même temps la série de gauche
	$gauche = [];
	$droite = [];
	foreach ($serie as $_id => $_valeur) {
		if ($_valeur < $valeur_coupe) {
			$gauche[] = $_valeur;
		} else {
			$droite = array_slice($serie, $_id);
			break;
		}
	}

	// Si les deux séries sont non vides on les renvoie
	if (
		(count($gauche) > 0)
		&& (count($droite) > 0)
	) {
		$series = [
			$gauche,
			$droite
		];
	}

	return $series;
}

/**
 * Transtype les valeurs d'une série dans un type numérique donné.
 * Si le type choisi n'est pas numérique aucun transtypage est effectué.
 *
 * @param array  $serie  Tableau des valeurs de la série.
 * @param string $format Type PHP ou équivalent. Prend les valeurs `int`, `integer`, bool`, `boolean`, `double`, `float` ou `floatxx`.
 * @param string $type
 *
 * @return array Série transtypée ou non si le type n'est pas valide.
 */
function serie_transtyper(array $serie, string $type) : array {
	// On passe toutes les valeurs de la série jusqu'à trouver l'index le plus proche de la valeur de coupe.
	// -- pour améliorer les performances, on constitue en même temps la série de gauche
	if (in_array($type, ['integer', 'int', 'bool', 'boolean'])) {
		// En entier
		$serie = array_map('intval', $serie);
	} elseif (
		(strpos($type, 'float') !== false)
		|| ($type === 'double')
	) {
		// En réel
		$serie = array_map('floatval', $serie);
	}

	return $serie;
}

/**
 * Transforme les valeurs d'une série en appliquant une fonction donnée à chaque valeur.
 *
 * @param array     $serie          Tableau des valeurs de la série.
 * @param array     $transformation Opération mathématique à appliquer à la valeur décrite sous forme de tableau :
 *                                  - string `fonction`  : fonction mathématique PHP à appliquer
 *                                  - float  `parametre` : le parametre éventuel de la fonction PHP autre que la valeur (null sinon)
 *                                  - bool   `inverser`  : indique si on doit inverser la valeur transformée
 * @param null|bool $reciproque     Indique si l'on veut appliquer la réciproque de la fonction décrite dans $transformation (défaut `false`).
 *
 * @return array Série transformée ou non si la fonction n'est pas valide.
 */
function serie_transformer(array $serie, array $transformation, ?bool $reciproque = false) : array {
	// On appelle la fonction de transformation pour chaque valeur de la série
	foreach ($serie as $_cle => $_valeur) {
		$serie[$_cle] = valeur_transformer($_valeur, $transformation, $reciproque);
	}

	return $serie;
}

/**
 * Transforme une valeur entière ou réelle en appliquant une fonction donnée.
 *
 * @param float|int  $valeur         Valeurs entière ou décimale à transformer.
 * @param null|array $transformation Opération mathématique à appliquer à la valeur décrite sous forme de tableau :
 *                                   - string `fonction`  : fonction mathématique PHP à appliquer
 *                                   - float  `parametre` : le parametre éventuel de la fonction PHP autre que la valeur (null sinon)
 *                                   - bool   `inverser`  : indique si on doit inverser la valeur transformée
 * @param null|bool  $reciproque     Indique si l'on veut appliquer la réciproque de la fonction décrite dans $transformation (défaut `false`).
 *
 * @return float Valeur transformée ou non si la fonction n'est pas valide.
 */
function valeur_transformer(int|float $valeur, array $transformation, ?bool $reciproque = false) : float {
	// Liste des fonctions applicables
	static $fonctions_autorisees = ['pow','sqrt', 'log', 'log10', 'exp', 'expa'];

	// Extraire la fonction et le paramètre éventuel
	$fonction = $transformation['fonction'] ?? '';
	$parametre = $transformation['parametre'] ?? null;

	// Initialiser la sortie à la valeur d'entrée et appliquer la fonction si besoin
	$valeur_transformee = $valeur;
	if (
		$fonction
		&& in_array($fonction, $fonctions_autorisees)
	) {
		if (!$reciproque) {
			// Appliquer la fonction sans se préoccuper de l'inversion
			// -- construire la liste des parametres incluant la valeur
			$parametres = array_filter([$valeur, $parametre]);
			$valeur_transformee = call_user_func_array($fonction, $parametres);
		} else {
			// Appliquer la fonction réciproque en gérant l'inversion éventuelle pour les logarithmes et exponentiels
			switch ($fonction) {
				case 'pow':
				case 'sqrt':
					$parametre = $fonction === 'sqrt' ? 2 : 1 / $parametre;
					$valeur_transformee = pow($valeur, $parametre);
					break;
				case 'log':
				case 'log10':
					if ($fonction === 'log10') {
						$parametre = 10;
					}
					$f = ($parametre === null) ? 'exp' : 'expa';
					if ($transformation['inverser']) {
						$valeur = 1 / $valeur;
					}
					$parametres = array_filter([$valeur, $parametre]);
					$valeur_transformee = call_user_func_array($f, $parametres);
					break;
				case 'exp':
				case 'expa':
					$valeur_transformee = ($transformation['inverser'] ? -1 : 1) * log($valeur, $parametre);
					$transformation['inverser'] = false;
					break;
			}

			// On désactive l'inversion qui a été traitée
			if (
				$fonction !== 'pow'
				&& $fonction !== 'sqrt'
			) {
				$transformation['inverser'] = false;
			}
		}
	}

	// Si l'inverse est demandé sur la fonction, on l'applique
	if (!empty($transformation['inverser'])) {
		$valeur_transformee = 1 / $valeur_transformee;
	}

	return $valeur_transformee;
}

/**
 * Calcule l'exponentielle pour une base donnée.
 *
 * @param float      $num  La valeur pour laquelle on calcule l'exponentielle
 * @param null|float $base La base optionnelle à utiliser (par défaut, `e`)
 *
 * @return float Valeur calculée
 */
function expa(float $num, ?float $base = M_E) : float {
	return pow($base, $num);
}
