<?php
/**
 * Ce fichier contient les fonctions de statistique descriptive.
 * Seules les séries quantitatives sont prises en compte (valeurs numériques).
 *
 * @package SPIP\EZMATH\STATISTIQUE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EZMATH_CORRECTION_BESSEL')) {
	/**
	 * Indique si la correction de Bessel doit être appliquée ou pas sur la variance (défaut `true`).
	 */
	define('_EZMATH_CORRECTION_BESSEL', true);
}

/**
 * Renvoie, pour une série quantitative donnée, la liste des indicateurs de position, de dispersion et de forme.
 * L'indice de Geary n'est pas renvoyé (requiert des données autres que la série elle-même).
 *
 * @param array $serie Tableau représentant la série
 *
 * @return array Liste des indicateurs statistiques (position, dispersion et forme)
 */
function serie_statistiques(array $serie) : array {
	// Initialisation des indicateurs généraux
	$indicateurs = [
		'effectif' => count($serie),
		'min'      => min($serie),
		'max'      => max($serie),
	];

	// Initialisation des indicateurs de position
	$position = [
		'moyenne'   => serie_moyenne($serie),
		'moyenne_g' => serie_moyenne_geometrique($serie),
		'moyenne_h' => serie_moyenne_harmonique($serie),
		'mediane'   => serie_mediane($serie),
	];
	$position = array_merge($position, serie_quartiles($serie));

	// Initialisation des indicateurs de dispersion
	$dispersion = [
		'etendue'  => $indicateurs['max'] - $indicateurs['min'],
		'variance' => serie_variance($serie),
		'iq'       => $position['q3'] - $position['q1'],
	];
	$dispersion['ecart_type'] = sqrt($dispersion['variance']);

	// Indicateurs de forme (variation, asymétrie, kurtosis)
	$forme = [
		'cv'                => $dispersion['ecart_type'] / $position['moyenne'],
		'asymetrie_fisher'  => serie_asymetrie_fisher($serie),
		'asymetrie_yule'    => serie_asymetrie_yule($serie),
		'asymetrie_pearson' => serie_asymetrie_pearson($serie),
		'kurtosis_fisher'   => serie_kurtosis_fisher($serie),
		'kurtosis_pearson'  => serie_kurtosis_pearson($serie),
	];

	return array_merge($indicateurs, $position, $dispersion, $forme);
}

// ------------------------------------------------------------------------
// ---------------------- INDICATEURS DE POSITION -------------------------
// ------------------------------------------------------------------------

/**
 * Calcule le mode d'une série de valeurs.
 * Le mode est la valeur distincte correspondant à l’effectif le plus élevé. Il n'est pas nécessairement unique.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|array Tableau des modes même si la série ne possède qu'un seul mode ou `null` si erreur (série vide).
 */
function serie_mode(array $serie) : null|array {
	// Exclure la série vide en renvoyant une erreur
	$modes = null;

	if (!empty($serie)) {
		// Pour compter les valeurs de la série on les passe en chaine pour appliquer ensuite la fonction PHP de comptage
		$serie_effectifs = array_count_values(array_map('strval', $serie));
		$effectif_max = max($serie_effectifs);

		// Il peut y avoir plusieurs modes, on retourne donc toujours un tableau
		$modes = [];
		foreach ($serie_effectifs as $_modalite => $_effectif) {
			if ($_effectif === $effectif_max) {
				$modes[] = $_modalite;
			}
		}

		// On repasse les valeurs en réels
		$modes = array_map('floatval', $modes);
	}

	return $modes;
}

/**
 * Calcule la moyenne arithmétique d'une série de valeurs.
 *
 *     ∑⟮xᵢ⟯
 * µ = ----- où xᵢ désigne la ième valeur de la série et n l'effectif de la série
 *       n
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Valeur de la moyenne ou `null` si erreur (série vide).
 */
function serie_moyenne(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$moyenne = null;

	if (!empty($serie)) {
		$moyenne = array_sum($serie) / count($serie);
	}

	return $moyenne;
}

/**
 * Calcule la moyenne géométrique d'une série de valeurs.
 *
 * G = ⁿ√∏⟮xᵢ⟯ où xᵢ désigne la ième valeur de la série et n l'effectif de la série
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Valeur de la moyenne ou `null` si erreur (série vide).
 */
function serie_moyenne_geometrique(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$moyenne = null;

	if (!empty($serie)) {
		$moyenne = pow(array_product($serie), 1 / count($serie));
	}

	return $moyenne;
}

/**
 * Calcule la moyenne harmonique d'une série de valeurs.
 *
 *        n
 * H = ------- où xᵢ désigne la ième valeur de la série et n l'effectif de la série
 *     ∑(1/xᵢ)
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Valeur de la moyenne ou `null` si erreur (série vide).
 */
function serie_moyenne_harmonique(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$moyenne = null;

	if (!empty($serie)) {
		$somme_inv = 0;
		foreach ($serie as $_valeur) {
			if ($_valeur == 0) {
				// éviter la division par zéro
				$somme_inv = null;
				break;
			} else {
				// On somme les inverses
				$somme_inv += 1 / $_valeur;
			}
		}

		// On finalise le calcul si il n'y a pas d'erreur
		if ($somme_inv !== null) {
			$moyenne = count($serie) / $somme_inv;
		}
	}

	return $moyenne;
}

/**
 * Calcule la moyenne d'une série de valeurs pondérée par une liste de poids.
 *
 *     ∑⟮wᵢxᵢ⟯
 * P = ------ où xᵢ désigne la ième valeur de la série et wᵢ le ième poids
 *      ∑⟮wᵢ⟯
 *
 * @param array $serie Tableau des valeurs de la série.
 * @param array $poids Tableau des poids de même taille que la série.
 *
 * @return null|float Valeur de la moyenne ou `null` si erreur (série vide ou liste de poids invalide).
 */
function serie_moyenne_ponderee(array $serie, array $poids) : null|float {
	// Exclure les séries vides ou incohérentes en renvoyant une erreur
	$moyenne = null;

	// Vérifier si les tableaux de données et de poids ont la même longueur et que la somme des poids n'est pas nulle
	if (
		!empty($serie)
		&& !empty($poids)
		&& (count($serie) === count($poids))
		&& ($somme_poids = array_sum($poids))
		&& ($somme_poids > 0)
	) {
		// Calcul de la somme des produits des données par leurs poids
		$moyenne = array_sum(
			array_map(
				function ($x, $w) {
					return $x * $w;
				},
				$serie,
				$poids
			)
		) / $somme_poids;
	}

	return $moyenne;
}

/**
 * Calcule la médiane d'une série de valeurs.
 *                         n+1
 * - si n est impair : M = --- ème valeur de la série
 *                          2
 *                                                     n              n
 * - si n est pair   : M = la moitié de la somme de la - ème et de la -+1 ème valeurs de la série
 *                                                     2              2.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Valeur de la médiane ou `null` si erreur (série vide).
 */
function serie_mediane(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$mediane = null;

	if (!empty($serie)) {
		// On classe la série en ordre croissant, les index seront réassignés
		sort($serie);
		if (count($serie) % 2 == 1) {
			// Effectif est impair
			$mediane = $serie[(count($serie) - 1) / 2];
		} else {
			// Effectif est pair
			$mediane = ($serie[count($serie) / 2 - 1] + $serie[count($serie) / 2]) / 2;
		}
	}

	return $mediane;
}

/**
 * Calcule le p-ième centile d'une série de valeurs.
 * La méthode utilisée correspond au type 6 de la fonction quantile() du langage R, recommandée par le NIST.
 *                                                     p(n+1)
 * 1. Le rang k est calculé de la façon suivante : k = ------
 *                                                      100
 * 2. La valeur k est ensuite séparée en deux valeurs, sa partie entière (e) et sa partie décimale (d).
 *    Le centile est ensuite déterminé selon la règle suivante (avec xᵢ la ième valeur observée dans la série ordonnée) :
 *    - si e = 0  alors le centile est v₁ (la première valeur observée)
 *    - si e >= n alors le centile est vₙ (la dernière valeur observée)
 *    - sinon le centile se calcule par interpolation linéaire et est égale à : vₑ + d*(vₑ₊₁ - vₑ).
 *
 * @param array $serie Tableau des valeurs de la série.
 * @param int   $p     Le centile à calculer.
 *
 * @return null|float Valeur du p-ième centile ou `null` si erreur (série vide)
 */
function serie_centile(array $serie, int $p) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$centile = null;

	if (!empty($serie)) {
		// On classe la série en ordre croissant, les index seront réassignés
		sort($serie);
		// Effectif de la série
		$n = count($serie);

		// On calcule le rang k (méthode recommandée par le National Institute of Standards and Technology - NIST)
		$rang_k = ($p / 100) * ($n + 1);

		// La valeur k est ensuite séparée en deux valeurs, sa partie entière (e) et sa partie décimale (d).
		// Le centile est ensuite déterminé selon la règle suivante (avec vi la ième valeur observée dans la série ordonnée) :
		// - si e=0 alors  le centile est v1 (la première valeur observée)
		// - si e>=n alors  le centile est vn (la dernière valeur observée)
		// - sinon le centile se calcule par interpolation linéaire et est égale à : ve + d*(ve+1 - ve)
		$rang_k_arrondi = floor($rang_k);
		if ($rang_k_arrondi == 0) {
			$centile = $serie[0];
		} elseif ($rang_k_arrondi >= $n) {
			$centile = $serie[$n - 1];
		} else {
			$valeur_inf = $serie[$rang_k_arrondi - 1];
			$valeur_sup = $serie[$rang_k_arrondi];
			$centile = $valeur_inf + ($valeur_sup - $valeur_inf) * ($rang_k - $rang_k_arrondi);
		}
	}

	return $centile;
}

/**
 * Calcule les quantiles d'une série de valeurs pour un ordre donné.
 * Les ordres les plus utilisés sont :
 * - 2   : médiane
 * - 4   : quartiles
 * - 5   : quintiles
 * - 10  : déciles
 * - 100 : centiles.
 *
 * @param array $serie Tableau des valeurs de la série.
 * @param int   $ordre Valeur de l'ordre
 *
 * @return null|array Tableau des ($ordre-1) quantiles pour l'ordre choisi ou `null` si erreur (série vide).
 */
function serie_quantiles(array $serie, int $ordre) : null|array {
	// Exclure la série vide en renvoyant une erreur
	$quantiles = null;

	// On accepte tous les ordres même si les plus utilisés sont :
	// -- les ordres 2 (médiane), 4 (quartiles), 5 (quintiles), 10 (déciles), 100 (centiles)
	if (!empty($serie)) {
		// On acquiert les ($ordre-1) quantiles.
		// -- pas d'erreur possible car on on a exclu la série vide
		$quantiles = [];
		$nb_quantiles = $ordre - 1;
		$intervalle = floor(100 / $ordre);
		for ($i = 1; $i <= $nb_quantiles; $i++) {
			$quantiles[] = serie_centile($serie, $intervalle * $i);
		}
	}

	return $quantiles;
}

/**
 * Calcule les quartiles d'une série de valeurs, c'est-à-dire les 3 quantiles d'ordre 4, nommés Q₁, Q₂ et Q₃.
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|array Tableau des 3 quartiles ou `null` si erreur (série vide).
 */
function serie_quartiles(array $serie) : null|array {
	// Exclure la série vide en renvoyant une erreur
	$quartiles = null;

	if (!empty($serie)) {
		$quartiles = serie_quantiles($serie, 4);
		// On normalise la sortie, les index s'appelant généralement `Qi`
		$quartiles = array_combine(['q1', 'q2', 'q3'], $quartiles);
	}

	return $quartiles;
}

// ------------------------------------------------------------------------
// -------------------- INDICATEURS DE DISPERSION ------------------------
// ------------------------------------------------------------------------

/**
 * Calcule l'étendue d'une série de valeurs.
 *
 * E = max(x) - min(x) où x est la variable représentée par la série de valeurs
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Valeur de l'étendu de la série ou `null` si erreur (série vide).
 */
function serie_etendue(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$etendue = null;

	if (!empty($serie)) {
		sort($serie);
		$etendue = $serie[count($serie) - 1] - $serie[0];
	}

	return $etendue;
}

/**
 * Calcule la distance interquartile.
 *
 * IQ = Q₃ - Q₁
 *
 * @param array $serie Tableau des valeurs de la série.
 *
 * @return null|float Distance interquartile ou `null` si erreur (série vide)
 */
function serie_interquartile(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$iq = null;

	if (!empty($serie)) {
		$quartiles = serie_quantiles($serie, 4);
		$iq = $quartiles[2] - $quartiles[0];
	}

	return $iq;
}

/**
 * Calcule la variance d'une série de valeurs.
 * Le calcul dépend du contexte choisi, à savoir, de tenir compte de la correction de Bessel pour les échantillons.
 * Pour changer le contexte il faut agir sur la constante `_EZMATH_CORRECTION_BESSEL` qui est à `true` par défaut.
 *
 *       ∑⟮xᵢ - μ⟯²
 *  σ² = --------- où xᵢ désigne la ième valeur de la série, μ la moyenne arithmétique et ν les degrés de liberté
 *           ν           qui coincide avec l'effectif de la série, n, ou avec n-1 (correction de Bessel)
 *
 * @param array $serie             Tableau des valeurs de la série.
 * @param bool  $correction_bessel Indique si la correction de Bessel doit être appliquée (défaut) ou pas
 *
 * @return null|float Variance (corrigée ou non) ou `null` si erreur (série vide ou réduite à un élément si Bessel)
 */
function serie_variance(array $serie, bool $correction_bessel = _EZMATH_CORRECTION_BESSEL) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$variance = null;

	if (!empty($serie)) {
		$moyenne = serie_moyenne($serie);
		$variance = 0;
		foreach ($serie as $_valeur) {
			$variance += pow($_valeur - $moyenne, 2);
		}
		$n = count($serie);
		$degres_liberte = $correction_bessel && ($n > 1) ? $n - 1 : $n;
		$variance = $variance / $degres_liberte;
	}

	return $variance;
}

/**
 * Calcule l'écart type d'une série de valeurs.
 * Le calcul dépend du contexte comme pour la variance.
 *
 * σ = √⟮σ²⟯ où σ² désigne la variance corrigée ou non.
 *
 * @param array $serie             Tableau des valeurs de la série.
 * @param bool  $correction_bessel Indique si la correction de Bessel doit être appliquée (défaut) ou pas
 *
 * @return null|float Ecart type (corrigé ou non) ou `null` si erreur (série vide ou réduite à un élément si Bessel)
 */
function serie_ecart_type(array $serie, bool $correction_bessel = _EZMATH_CORRECTION_BESSEL) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$ecart_type = null;

	if (!empty($serie)) {
		$ecart_type = sqrt(serie_variance($serie, $correction_bessel));
	}

	return $ecart_type;
}

/**
 * Calcule le moment à l'origine ou centré d'ordre `p`, pour `p` supérieur à 1.
 *
 * - Moment à l'origine :
 *          ∑⟮xᵢ⟯ᴾ
 *   m'ₚ = ------- où xᵢ désigne la ième valeur de la série et p l'ordre
 *            n
 * - Moment centré :
 *         ∑⟮xᵢ - µ⟯ᴾ
 *   m'ₚ = --------- où xᵢ désigne la ième valeur de la série, µ la moyenne arithmétique et p l'ordre
 *             n
 *
 * @note
 *   Le moment m'₁ correspond à la moyenne µ et le moment centré m₂ à la variance (non corrigée).
 *
 * @param array     $serie  Tableau des valeurs de la série.
 * @param int       $ordre  Valeur de d'ordre du moment (puissance)
 * @param null|bool $centre Indique si on veut le moment à l'origine ou le moment centré (défaut)
 *
 * @return null|float Moment centré ou pas d'ordre p ou `null` si erreur (série vide)
 */
function serie_moment(array $serie, int $ordre, ?bool $centre = true) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$moment = null;

	if (!empty($serie)) {
		// On calcule la moyenne si l'on veut le moment centré
		$moyenne = $centre ? serie_moyenne($serie) : 0;
		$moment = 0;
		foreach ($serie as $_valeur) {
			$moment += pow($_valeur - $moyenne, $ordre);
		}
		$moment = $moment / count($serie);
	}

	return $moment;
}

// ---------------------------------------------------------------------
// ---------------------- INDICATEURS DE FORME -------------------------
// ---------------------------------------------------------------------

/**
 * Calcule le coefficient d'asymétrie de Fisher à partir du moment centré d'ordre 3.
 *
 *       m₃
 * γ₁ = ---- où m₃ est le moment centré d'ordre 3 et σ l'écart type de la série de valeurs.
 *       σ³
 *
 * L'interprétation du coefficient est le suivant:
 * - γ₁ = 0 pour une distribution symétrique,
 * - γ₁ > 0 pour une distribution asymétrique à gauche,
 * - γ₁ < 0 pour une distribution asymétrique à droite.
 *
 * @param array $serie
 *
 * @return null|float
 */
function serie_asymetrie_fisher(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$coefficient = null;

	if (!empty($serie)) {
		$coefficient = serie_moment($serie, 3, true) / pow(serie_ecart_type($serie), 3);
	}

	return $coefficient;
}

/**
 * Calcule le coefficient d'asymétrie de Yule à partir des quartiles.
 *
 *      (Q₃ + Q₁ - 2Q₂ )
 * Yₖ = ----------------- où Qᵢ désigne chacun des 3 quartiles.
 *         (Q₃ - Q₁)
 *
 * L'interprétation est la même que pour le coefficient de Fisher sachant que -1 ≤ Yₖ ≤ 1.
 *
 * @param array $serie
 *
 * @return null|float
 */
function serie_asymetrie_yule(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$coefficient = null;

	if (!empty($serie)) {
		// On récupère les quartiles
		$quartiles = serie_quartiles($serie);
		// On calcule le coefficient à partir de ces quartiles
		$coefficient = ($quartiles['q3'] + $quartiles['q1'] - 2 * $quartiles['q2']) / ($quartiles['q3'] - $quartiles['q1']);
	}

	return $coefficient;
}

/**
 * Calcule le coefficient d'asymétrie de Pearson à partir de la médiane (asymétrie de médiane).
 *
 *      3(µ - M)
 * Sₖ = -------- où µ désigne la moyenne arithmétique, M la médiane et σ l'écart type.
 *         σ
 *
 * L'interprétation est la même que pour le coefficient de Fisher sachant que -1 ≤ Sₖ ≤ 1.
 *
 * @param array $serie
 *
 * @return null|float
 */
function serie_asymetrie_pearson(array $serie) : null|float {
	// Exclure la série vide et les modes multiples en renvoyant une erreur
	$coefficient = null;

	if (!empty($serie)) {
		// On calcule le coefficient (version normalisée)
		$coefficient = 3 * (serie_moyenne($serie) - serie_mediane($serie)) / serie_ecart_type($serie);
	}

	return $coefficient;
}

/**
 * Calcule le coefficient d'aplatissement - kurtosis - de Pearson à partir du moment centré d'ordre 4.
 *
 *       m₄
 * β₂ = ---- où m₄ désigne le moment centré d'ordre 4 et σ l'écart type.
 *       σ⁴
 *
 * Le coefficient β₂ >= 1 et oscille autour de 3 pour une distribution normale. C'est pourquoi on utilise souvent
 * le coefficient de Fisher.
 *
 * @param array $serie
 *
 * @return null|float
 */
function serie_kurtosis_pearson(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$kurtosis = null;

	if (!empty($serie)) {
		$kurtosis = serie_moment($serie, 4, true) / pow(serie_variance($serie), 2);
	}

	return $kurtosis;
}

/**
 * Calcule le coefficient d'aplatissement - kurtosis - de Fisher qui normalise le coefficient d'aplatissement de Pearson.
 *
 * γ₂ = β₂ - 3
 *
 * L'interprétation du coefficient est le suivant:
 * - γ₂ = 0 kurtosis normale (mésokurtique), la distribution a une forme similaire à une distribution normale
 * - γ₂ > 0 kurtosis positif (leptokurtique), la distribution est plus concentrée au centre avec des queues épaisses
 * - γ₂ < 0 kurtosis négatif (platykurtique), la distribution est plus aplatie avec des queues fines
 *
 * @param array $serie
 *
 * @return null|float
 */
function serie_kurtosis_fisher(array $serie) : null|float {
	// Exclure la série vide en renvoyant une erreur
	$kurtosis = null;

	if (!empty($serie)) {
		$kurtosis = serie_kurtosis_pearson($serie) - 3;
	}

	return $kurtosis;
}

// ------------------------------------------------------------------------
// ------------------- INDICATEURS D'AUTOCORRELATION ----------------------
// ------------------------------------------------------------------------

/**
 * Calcule l'indice de Geary qui mesure l'autocorrélation spatiale qui exprime la corrélation des observations
 * adjacentes d'un même phénomène.
 *
 *     (N-1) ∑ ∑wᵢⱼ⟮xᵢ - xⱼ⟯²
 *           i j
 * C = --------------------- où N désigne le nombre de valeurs de la série x, (wᵢⱼ) la matrice de pondération spatiale
 *      2W∑⟮xᵢ - µ⟯²          et W la somme des wᵢⱼ
 *
 * La valeur de l'indice de Geary s'étend de 0 à 2 :
 * - = 1 : signifie qu'aucune autocorrélation spatiale n'est présente dans les mesures effectuées.
 * - < 1 : signifie une autocorrélation spatiale positive (valeurs similaires sont proches les unes des autres).
 * - > 1 : signifie une autocorrélation spatiale négative (valeurs dissimilaires sont proches les unes des autres).
 *
 * @param array $serie       Tableau des valeurs de la série.
 * @param array $ponderation Matrice de pondération spatiale
 *
 * @return null|float Indice ou null si erreur (série ou coordonnées vides ou incohérents)
 */
function serie_autocorrelation_geary(array $serie, array $ponderation) : null|float {
	// Exclure la série vide ou les coordonnées vides ou incohérents avec la série en renvoyant une erreur
	$indice = null;

	if (
		!empty($serie)
		&& !empty($ponderation)
		&& (count($ponderation) === count($serie))
	) {
		// Calcul l'effectif de la série
		$effectif = count($serie);

		// Calcul de la moyenne des valeurs
		$moyenne = serie_moyenne($serie);

		// Calcul du numérateur, du dénominateur de la formule et de la somme des poids.
		// -- on considère que le tableau de coordonnées peut avoir des clés numériques ou autres.
		$numerateur = 0;
		$denominateur = 0;
		$W = 0;
		foreach ($serie as $_i => $_serie_i) {
			foreach ($serie as $_j => $_serie_j) {
				// Numérateur : Somme pondérée des différences quadratiques
				$numerateur += $ponderation[$_i][$_j] * pow($_serie_i - $_serie_j, 2);

				// Accumuler la somme des pondérations
				$W += $ponderation[$_i][$_j];
			}

			// Dénominateur : Variance totale des valeurs
			$denominateur += pow($_serie_i - $moyenne, 2);
		}

		// Calcul de l'indice de Geary
		$indice = (($effectif - 1) * $numerateur) / (2 * $W * $denominateur);
	}

	return $indice;
}
