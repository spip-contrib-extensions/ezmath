<?php
/**
 * Ce fichier contient l'API de gestion des couleurs.
 *
 * @package SPIP\EZMATH\COULEUR\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Linéarise un tableau de coordonnées sRGB en supprimant la correction gamma.
 * Les coordonnées RGB sont fournies normalisées, à savoir, que leur valeur est comprise dans l'intervalle [0, 1].
 *
 * @param array $srgb Coordonnées RGB dont les valeurs absolues "in-gamut" sont dans l'intervalle [0.0, 1.0]
 *
 * @return array Les coordonnées linéarisées
 */
function _srgb_lineariser(array $srgb) : array {
	$lineariser = function (float $coordonnee) : float {
		$signe = $coordonnee < 0 ? -1 : 1;
		$coordonnee_absolue = abs($coordonnee);

		if ($coordonnee_absolue < 0.04045) {
			$coordonnee_linearisee = $coordonnee / 12.92;
		} else {
			$coordonnee_linearisee = $signe * (pow(($coordonnee_absolue + 0.055) / 1.055, 2.4));
		}

		return $coordonnee_linearisee;
	};

	return array_map($lineariser, $srgb);
}

/**
 * Effectue la correction gamma des coordonnées sRBG linéarisées.
 * Les coordonnées RGB sont fournis normalisées, à savoir, que leur valeur est comprise dans l'intervalle [0, 1].
 *
 * @param array $srgb Tableau RGB dont les valeurs absolues in-gamut sont dans l'intervalle [0.0, 1.0]
 *
 * @return array Les coordonnées corrigées en gamma
 */
function _srgb_corriger_gamma(array $srgb) : array {
	$corriger = function (float $coordonnee) : float {
		$signe = $coordonnee < 0 ? -1 : 1;
		$coordonnee_absolue = abs($coordonnee);

		if ($coordonnee_absolue > 0.0031308) {
			$coordonnee_linearisee = $signe * (1.055 * pow($coordonnee_absolue, 1 / 2.4) - 0.055);
		} else {
			$coordonnee_linearisee = $coordonnee * 12.92;
		}

		return $coordonnee_linearisee;
	};

	return array_map($corriger, $srgb);
}

/**
 * Renvoie une couleur au format hexadécimal construite à partir de deux couleurs RGB.
 * Il est possoible de choisir la méthode de mélange et donc les coefficients à appliquer.
 *
 * @api
 *
 * @param null|string $couleur1 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur bleu.
 *                              Cette couleur est réputée la plus sombre
 * @param null|string $couleur2 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur rouge.
 *                              Cette couleur est réputée la plus sombre
 * @param null|array  $options  Options pour le mélange des couleurs.
 *
 * @return string Couleur au format hexadécimal (#aabbcc)
 */
function couleur_melanger(?string $couleur1 = 'yellow', ?string $couleur2 = 'brown', ?array $options = []) : string {
	// Traduire les couleurs hexa en RGB (tanleau 'red', 'green', 'blue')
	include_spip('inc/filtres_images_mini');
	$rgb1 = _couleur_hex_to_dec($couleur1);
	$rgb2 = _couleur_hex_to_dec($couleur2);

	// Méthode de mélange
	if (!isset($options['methode'])) {
		$options['methode'] = 'no_coefficient';
	}

	// Calcul de la couleur résultante en fonction des paramètres
	foreach (['red', 'green', 'blue'] as $_couleur_primaire) {
		// Application de la méthode choisie sur les coefficients à appliquer à chaque couleur
		if (!in_array($options['methode'], ['unique_coefficient', 'rgb_coefficient'])) {
			$coefficient = [1, 1];
		} else {
			if ($options['methode'] === 'unique_coefficient') {
				$coefficient[0] = $options['coefficient'] ?: 0.5;
			} else {
				$coefficient[0] = $options['coefficient'][$_couleur_primaire] ?: 0.5;
			}
			$coefficient[1] = 1 - $coefficient[0];
		}

		// Additionner les deux couleurs et s'assurer que les valeurs sont bien dans l'intervalle [0, 255]
		$rgb[$_couleur_primaire] = $rgb1[$_couleur_primaire] * $coefficient[0] + $rgb2[$_couleur_primaire] * $coefficient[1];
		$rgb[$_couleur_primaire] = max(0, min(255, round($rgb[$_couleur_primaire])));
	}

	$couleur = '#' . _couleur_dec_to_hex($rgb['red'], $rgb['green'], $rgb['blue']);

	return $couleur;
}
