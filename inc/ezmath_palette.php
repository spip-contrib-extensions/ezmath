<?php
/**
 * Ce fichier contient l'API de gestion des palettes de couleurs.
 *
 * @package SPIP\EZMATH\PALETTE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie une palette de couleurs calculée à partir d'une couleur de base et de couleurs dérivées en modifiant uniquement
 * la luminosité de façon linéaire (single hue progression).
 *
 * @api
 *
 * @param null|string $couleur        Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur #000 (noir).
 *                                    Cette couleur est réputée la plus sombre
 * @param null|int    $taille         Nombre de couleurs de la palette. Par défaut, prend la valeur 7.
 * @param null|float  $luminosite_min Identifiant d'un champ ou de plusieurs champs de la description d'un territoire.
 * @param null|float  $luminosite_max Identifiant d'un champ ou de plusieurs champs de la description d'un territoire.
 *
 * @return array Tableau des couleurs hexa de la palette
 */
function palette_teinte_unique(?string $couleur = '#000', ?int $taille = 7, ?float $luminosite_min = 0, ?float $luminosite_max = 0) : array {
	// On renvoie le tableau des couleurs en hexa
	$palette = [];

	// Traduire les couleurs hexa en HSL
	include_spip('inc/filtres_images_mini');
	$hsl = _couleur_hex_to_hsl($couleur);

	// Extraire chaque élément de la couleur
	$teinte = $hsl['h'];
	$saturation = $hsl['s'];
	$luminosite = $hsl['l'];

	// Définir l'intervalle de variation de la luminosité
	if (!$luminosite_max) {
		$luminosite_max = $luminosite;
		if (!$luminosite_min) {
			$luminosite_min = 0.95;
		}
	} elseif (!$luminosite_min) {
		$luminosite_min = $luminosite;
	}
	// Si le min et le maw sont inversés on les rérganise de façon à toujours avoir une palette progressive en
	// luminosité
	if ($luminosite_max < $luminosite_min) {
		$luminosite_min_reel = $luminosite_max;
		$luminosite_max = $luminosite_min;
		$luminosite_min = $luminosite_min_reel;
	}

	// Définir le pas de variaton de la luminosité
	$pas_luminosite = $taille === 1 ? 0 : ($luminosite_max - $luminosite_min) / ($taille - 1);

	// Création de la palette
	for ($i = 0; $i < $taille; $i++) {
		$palette[] = '#' . _couleur_hsl_to_hex($teinte, $saturation, $luminosite_min + $pas_luminosite * $i);
	}

	return $palette;
}

/**
 * Renvoie une palette de couleurs calculée à partir d'une couleur de base et de couleurs dérivées en modifiant uniquement
 * la luminosité de façon linéaire (single hue progression).
 *
 * @api
 *
 * @param null|string $couleur1 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur bleu.
 *                              Cette couleur est réputée la plus sombre
 * @param null|string $couleur2 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur rouge.
 *                              Cette couleur est réputée la plus sombre
 * @param null|int    $taille   Nombre de couleurs de la palette. Par défaut, prend la valeur 7.
 *
 * @return array Tableau des couleurs hexa de la palette
 */
function palette_teinte_bipolaire(?string $couleur1 = '#0000ff', ?string $couleur2 = '#ff0000', ?int $taille = 7) : array {
	// Calculer une progression à teinte unique pour chaque couleur pour la moitié de la taille voulue pour la palette
	$palette1 = palette_teinte_unique($couleur1, (int) ($taille / 2) + 1, null, 1);
	$palette2 = palette_teinte_unique($couleur2, (int) ($taille / 2) + 1, 1);

	// Si la taille de la palette est impaire, on supprime la couleur la plus claire de la première palette
	if ($taille % 2 !== 0) {
		array_pop($palette1);
	}

	// On concatène les deux palettes
	$palette = array_merge($palette1, array_reverse($palette2));

	return $palette;
}

/**
 * Renvoie une palette de couleurs calculée à partir .
 *
 * @api
 *
 * @param null|string $couleur1 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur bleu.
 *                              Cette couleur est réputée la plus sombre
 * @param null|float  $teinte2  Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur rouge.
 *                              Cette couleur est réputée la plus sombre
 * @param null|int    $taille   Nombre de couleurs de la palette. Par défaut, prend la valeur 7.
 *
 * @return array Tableau des couleurs hexa de la palette
 */
function palette_teinte_melangee(?string $couleur1 = 'brown', ?float $teinte2 = 0.95, ?int $taille = 7) : array {
	// Traduire les couleurs hexa en HSL
	include_spip('inc/filtres_images_mini');
	$hsl1 = _couleur_hex_to_hsl($couleur1);

	// Extraire les éléments de la couleur de base
	$teinte1 = $hsl1['h'];
	$saturation = $hsl1['s'];
	$luminosite = $hsl1['l'];

	// Calcul du pas des couleurs
	$pas = abs(($teinte2 - $teinte1) / $taille);

	// Constitution de la palette
	for ($i = 0; $i < $taille - 1; $i++) {
		$teinte = ($teinte1 + $pas * $i);
		$palette[] = '#' . _couleur_hsl_to_hex($teinte, $saturation, $luminosite);
	}

	return $palette;
}

/**
 * Renvoie une palette de couleurs calculée à partir .
 *
 * @api
 *
 * @param null|string $couleur1 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur bleu.
 *                              Cette couleur est réputée la plus sombre
 * @param null|string $couleur2 Couleur RGB exprimée sous la forme hexadécimal #xxxxxx. Par défaut, prend la valeur rouge.
 *                              Cette couleur est réputée la plus sombre
 * @param null|int    $taille   Nombre de couleurs de la palette. Par défaut, prend la valeur 7.
 *
 * @return array Tableau des couleurs hexa de la palette
 */
function palette_couleur_melangee(?string $couleur1 = 'yellow', ?string $couleur2 = 'brown', ?int $taille = 7) : array {
	// Initialiser la palette avec la couleur 1
	$palette[] = $couleur1;

	// Détermination de la méthode
	$options = [
		'methode' => 'unique_coefficient'
	];

	// Insertion des couleurs intermédiaires
	include_spip('inc/ezmath_couleur');
	for ($i = 1; $i < $taille - 1; $i++) {
		$options['coefficient'] = 1 - ($i / $taille);
		$palette[] = couleur_melanger($couleur1, $couleur2, $options);
	}

	// Finalisation de la palette avec la couleur 2
	$palette[] = $couleur2;

	return $palette;
}
