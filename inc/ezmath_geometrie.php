<?php
/**
 * Ce fichier contient l'API des fonctions standard de géométrie.
 *
 * @package SPIP\EZMATH\GEOMETRIE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_EZMATH_RAYON_TERRE_KM')) {
	/**
	 * Rayon de la terre en kilomètre selon l'UGGI.
	 */
	define('_EZMATH_RAYON_TERRE_KM', 6371.009);
}

/**
 * Calcule la distance euclidienne entre deux points connus par leur coordonnées cartésiennes dans un espace à n dimensions.
 *
 * d(p,q) = √∑(pᵢ - qᵢ)² où pᵢ et qᵢ désignent les ième coordonnées des points p et q.
 *
 * @param array $point_p Coordonnées du premier point
 * @param array $point_q Coordonnées du deuxième point
 *
 * @return null|float Valeur de la distance euclidienne ou null si les points ne sont pas dans le même espace vectoriel
 */
function distance_euclidienne(array $point_p, array $point_q) : null|float {
	// Exclure les points vides ou exprimés dans des espaces de dimension différente
	$distance = null;

	if (
		!empty($point_p)
		&& !empty($point_q)
		&& (count($point_p) === count($point_q))
	) {
		if (count($point_p) === 1) {
			// Espace unidimensionnel : la distance est juste la différence absolue entre les deux uniques coordonnées
			$distance = abs($point_p[0] - $point_q[0]);
		} else {
			// Espace multidimentionnel
			$distance = 0;
			for ($i = 0; $i < count($point_p); $i++) {
				$distance += pow($point_p[$i] - $point_q[$i], 2);
			}
			// Finalisation du calcul
			$distance = sqrt($distance);
		}
	}

	return $distance;
}

/**
 * Calcule la distance géodésique en utilisant la formule d'Haversine qui permet de déterminer la distance du grand cercle
 * entre deux points d'une sphère, à partir de leurs longitudes et latitudes.
 * Latitude et longitude sont exprimés en degrés (non sexagésimaux).
 *                        √a                  φ₂-φ₁                         λ₂-λ₁
 * d(p₁,p₂) = 2r.arctan(------) avec a = sin²(-----) + cos(φ₁).cos(φ₂).sin²(-----)
 *                      √(1-a)                  2                             2
 *                              où r est le rayon de la terre, soit 6371,009 km
 *                                 φ₁ et φ₂ sont les latitudes des points p₁ et p₂
 *                                 λ₁ et λ₂ sont les longitudes des points p₁ et p₂
 *
 * Cette formule est plus précise grâce à l'utilisation de la fonction arctangente que celle classique avec l'arcsinus.
 *
 * @param array $point1 Coordonnées du premier point exprimé en (latitude, longitude)
 * @param array $point2 Coordonnées du deuxième pointexprimé en (latitude, longitude)
 *
 * @return null|float Valeur de la distance euclidienne ou null si les points ne sont pas dans le même espace vectoriel
 */
function sphere_distance_haversine(array $point1, array $point2) : null|float {
	// Exclure les points vides ou exprimés dans des espaces de dimension différente
	$distance = null;

	if (
		!empty($point1)
		&& !empty($point2)
		&& (count($point1) === count($point2))
	) {
		// Convertir les coordonnées de degrés en radians
		$point1 = array_map('deg2rad', $point1);
		$point2 = array_map('deg2rad', $point2);

		// Calcul des deltas
		$delta_latitude = $point2[0] - $point1[0];
		$delta_longitude = $point2[1] - $point1[1];

		// Calcul de la variable a
		$a = pow(sin($delta_latitude / 2), 2)
			+ pow(sin($delta_longitude / 2), 2) * cos($point1[0]) * cos($point2[0]);

		// Finalisation du calcul
		$distance = 2 * _EZMATH_RAYON_TERRE_KM * atan2(sqrt($a), sqrt(1 - $a));
	}

	return $distance;
}

/**
 * Calcule la distance entre deux points d'une sphère en utilisant la loi des sinus à partir de leurs longitudes et latitudes.
 * Latitude et longitude sont exprimés en degrés (non sexagésimaux).
 *
 * d(p₁,p₂) = r.arccos(cos_β) avec cos_β = sin(φ₁).sin(φ₂) + cos(φ₁).cos(φ₂).cos(λ₂-λ₁), β étant appelé l'angle central
 *                            où r est le rayon de la terre, soit 6371,009 km
 *                               φ₁ et φ₂ sont les latitudes des points p₁ et p₂
 *                               λ₁ et λ₂ sont les longitudes des points p₁ et p₂
 *
 * @param array $point1 Coordonnées du premier point exprimé en (latitude, longitude)
 * @param array $point2 Coordonnées du deuxième pointexprimé en (latitude, longitude)
 *
 * @return null|float Valeur de la distance euclidienne ou null si les points ne sont pas dans le même espace vectoriel
 */
function sphere_distance_loi_sinus(array $point1, array $point2) : null|float {
	// Exclure les points vides ou exprimés dans des espaces de dimension différente
	$distance = null;

	if (
		!empty($point1)
		&& !empty($point2)
		&& (count($point1) === count($point2))
	) {
		// Convertir les coordonnées de degrés en radians
		$point1 = array_map('deg2rad', $point1);
		$point2 = array_map('deg2rad', $point2);

		// Calcul des deltas
		$delta_longitude = $point2[1] - $point1[1];

		// Calcul l'angle central ou plutôt le cosinus de cet angle
		$cos_beta = sin($point1[0]) * sin($point2[0]) + cos($point1[0]) * cos($point2[0]) * cos($delta_longitude);

		// Finalisation du calcul : d = r * β
		$distance = _EZMATH_RAYON_TERRE_KM * acos($cos_beta);
	}

	return $distance;
}

/**
 * Calcule la distance euclidienne entre deux points d'une sphère, à partir de leurs longitudes et latitudes.
 * Latitude et longitude sont exprimés en degrés (non sexagésimaux).
 *
 * Les coordonnées sphériques (φ,λ) sont convertis en coordonnées cartésiennes (x,y) de la façon suivante:
 * - x = r.cos(φ).cos(λ)
 * - y = r.cos(φ).sin(λ) où r est le rayon de la terre, soit 6371,009 km
 * Il suffit alors de calculer la distance euclidienne d(p, q).
 *
 * Ce calcul devient très approximatif quand les points sont éloignés.
 *
 * @param array $point1 Coordonnées du premier point exprimé en (latitude, longitude)
 * @param array $point2 Coordonnées du deuxième pointexprimé en (latitude, longitude)
 *
 * @return null|float Valeur de la distance euclidienne ou null si les points ne sont pas dans le même espace vectoriel
 */
function sphere_distance_euclidienne(array $point1, array $point2) : null|float {
	// Exclure les points vides ou exprimés dans des espaces de dimension différente
	$distance = null;

	if (
		!empty($point1)
		&& !empty($point2)
		&& (count($point1) === count($point2))
	) {
		// Convertir les coordonnées de degrés en radians
		$point1 = array_map('deg2rad', $point1);
		$point2 = array_map('deg2rad', $point2);

		// Convertir les coordonnées sphériques en coordonnées cartésiennes
		$latitude = $point1[0];
		$longitude = $point1[1];
		$point1 = [
			_EZMATH_RAYON_TERRE_KM * cos($latitude) * cos($longitude),
			_EZMATH_RAYON_TERRE_KM * cos($latitude) * sin($longitude),
		];
		$latitude = $point2[0];
		$longitude = $point2[1];
		$point2 = [
			_EZMATH_RAYON_TERRE_KM * cos($latitude) * cos($longitude),
			_EZMATH_RAYON_TERRE_KM * cos($latitude) * sin($longitude),
		];

		// Calcul de la distance euclidienne
		$distance = distance_euclidienne($point1, $point2);
	}

	return $distance;
}

/**
 * Calcule la matrice de pondération spatiale d'une liste de coordonnées ex^primés en (latitude, longitude).
 * Latitude et longitude sont exprimés en degrés (non sexagésimaux).
 *
 * @param array       $coordonnees Coordonnées des points exprimés en (latitude, longitude)
 * @param null|string $f_distance  Fonction de calcul de la distance entre points. Par défaut méthode d'Haversine
 *
 * @return null|float Valeur de la distance euclidienne ou null si les points ne sont pas dans le même espace vectoriel
 */
function matrice_ponderation_spatiale(array $coordonnees, ?string $f_distance = 'sphere_distance_haversine') : array {
	// Initialisation de la matrice de sortie et extraction du nombre de points
	$matrice = [];

	// Paramètre pour éviter division par zéro
	$epsilon = 1e-6;

	// On considère que le tableau de coordonnées peut avoir des clés numériques ou autres.
	foreach ($coordonnees as $_i => $_coordonnees_i) {
		// Initialisation de la ligne
		$matrice[$_i] = [];

		foreach ($coordonnees as $_j => $_coordonnees_j) {
			if ($_i === $_j) {
				// Pas de pondération entre une unité spatiale et elle-même
				$matrice[$_i][$_j] = 0;
			} else {
				// Calcul de la distance selon la fonction choisie
				$distance = $f_distance($_coordonnees_i, $_coordonnees_j);

				// Pondération inverse de la distance
				$matrice[$_i][$_j] = 1 / ($distance + $epsilon);
			}
		}
	}

	return $matrice;
}
